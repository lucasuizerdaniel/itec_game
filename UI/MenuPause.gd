extends Control

signal continuePlaying

func _on_btn_continue_pressed():
	emit_signal("continuePlaying")

func _on_btn_quit_pressed():
	Engine.time_scale=1
	get_tree().call_group("projectiles","queue_free")
	get_tree().call_group("pickups","queue_free")
	get_tree().change_scene_to_file("res://UI/MenuMain.tscn")
