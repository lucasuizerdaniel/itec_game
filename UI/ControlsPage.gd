extends Control

func _ready():
	$Cache/Player.stopCompletely()

func _on_btn_play_pressed():
	get_tree().call_group("projectiles","queue_free")
	get_tree().call_group("pickups","queue_free")
	get_tree().change_scene_to_file("res://Game.tscn")
