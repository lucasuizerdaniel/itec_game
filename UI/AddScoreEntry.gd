extends Control

var scoreToSubmit: int=0

func setScore(score: int):
	scoreToSubmit=score
	$lblValue.text=Global.convertToTime(score)

func _on_btn_submit_pressed():
	var entry: ScoreEntry=ScoreEntry.new()
	entry.playerName=$lneName.text
	entry.playerScore=scoreToSubmit
	DataManager.addScore(entry)
	#Make same stuff that MenuGameOver when button quit is pressed
	get_tree().call_group("projectiles","queue_free")
	get_tree().call_group("pickups","queue_free")
	get_tree().change_scene_to_file("res://UI/MenuMain.tscn")

func _input(event):
	if(event.is_action_pressed("ui_accept")):
		_on_btn_submit_pressed()
