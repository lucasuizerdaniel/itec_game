extends Panel
class_name ScoreEntry

@onready var playerPosition: int=0
@onready var playerName: String="NAME"
@onready var playerScore: int=0

func update(data: ScoreEntry) ->void:
	#Update variables
	playerPosition=data.playerPosition
	playerName=data.playerName
	playerScore=data.playerScore
	#update labels
	$lblPosition.text=str(playerPosition)
	$lblName.text=playerName
	$lblValue.text=Global.convertToTime(playerScore)
