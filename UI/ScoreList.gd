extends Control

func _ready():
	DataManager.loadScoreEntries()
	for i in 10:
		$VBoxContainer.get_child(i).update(DataManager.scoreEntries[i])

func _on_btn_quit_pressed():
	get_tree().change_scene_to_file("res://UI/MenuMain.tscn")
