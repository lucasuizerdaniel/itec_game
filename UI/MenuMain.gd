extends Control

func _on_btn_play_pressed():
	$AnimationPlayer.play("start")
	AudioManager.playSoundSFX("res://Assets/Sounds/MenuMain Weapon Grab.wav")

func _on_btn_exit_pressed():
	get_tree().quit()

func _on_btn_score_pressed():
	get_tree().change_scene_to_file("res://UI/ScoreList.tscn")

func startNewGame()-> void:
	get_tree().change_scene_to_file("res://UI/ControlsPage.tscn")

func _on_btn_credits_pressed():
	get_tree().change_scene_to_file("res://UI/Credits.tscn")
