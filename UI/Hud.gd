extends Control

@export var ammoReserve: AmmoReserve
var ammoTypeFocused: Global.AMMO_TYPES=Global.AMMO_TYPES.INFINITY

func _ready():
	ammoReserve.connect("ammo_reserve_changed",updateAmmoReserveCounter)
	Global.connect("score_changed",updateScore)

func _physics_process(_delta):
	$FPS.text=str(Engine.get_frames_per_second())

func updateScore(score) ->void:
	$lblScore.text=Global.convertToTime(score)

#Player HP
func _on_health_hp_changed(current):
	$Status/BarHealth.value=current

#Process weapon switch
func _on_weapon_selector_weapon_switched(weapon):
	if(!weapon._magazine.is_connected("ammo_changed",updateAmmoCounter)):
		weapon._magazine.connect("ammo_changed",updateAmmoCounter)
	ammoTypeFocused=weapon._magazine.ammoType
	updateAmmoCounter(weapon._magazine.ammoCurrent,weapon._magazine.ammoType)

#Counter ammo
func updateAmmoCounter(currentAmmo,type) ->void:
	$Status/AmmoCounter/lblAmmo.text=str(currentAmmo)+" / "
	updateAmmoReserveCounter(type)

func updateAmmoReserveCounter(type) ->void:
	if(type==ammoTypeFocused):
		if(type==Global.AMMO_TYPES.INFINITY):
			$Status/AmmoCounter/lblReserve.text="INF"
		else:
			$Status/AmmoCounter/lblReserve.text=str(ammoReserve.getAmmo(type))


