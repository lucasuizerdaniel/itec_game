extends Control

signal play_again

func _on_btn_reset_pressed():
	emit_signal("play_again")

func _on_btn_quit_pressed():
	get_tree().call_group("projectiles","queue_free")
	get_tree().call_group("pickups","queue_free")
	get_tree().change_scene_to_file("res://UI/MenuMain.tscn")

