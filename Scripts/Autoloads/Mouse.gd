extends Node

var crosshairAttack=preload("res://Assets/Crosshair_attack.png")
var crosshairReload=preload("res://Assets/Crosshair_reload.png")

func _ready() ->void:
	changeMouseToAttack()

func changeMouseToAttack() ->void:
	Input.set_custom_mouse_cursor(crosshairAttack,Input.CURSOR_ARROW,Vector2(16,16))

func changeMouseToReload() ->void:
	Input.set_custom_mouse_cursor(crosshairReload,Input.CURSOR_ARROW,Vector2(16,16))
