extends Node

var playerSFX: AudioStreamPlayer=AudioStreamPlayer.new()

func _ready():
	add_child(playerSFX)

func playSoundSFX(soundPath: String)-> void:
	if(playerSFX.playing):
		playerSFX.stop()
	playerSFX.stream=load(soundPath)
	playerSFX.play()
