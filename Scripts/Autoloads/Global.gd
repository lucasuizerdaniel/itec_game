extends Node

enum AMMO_TYPES {INFINITY=0,BULLETS,SHELLS,EXPLOSIVES,MAX}
#Score stuff
var score: int=0
var scoreTimer: Timer=Timer.new()
signal score_changed(newScore: int)

func _ready()-> void:
	add_child(scoreTimer)
	scoreTimer.wait_time=1
	scoreTimer.connect("timeout",score_timer_timeout)

#Score functions
func resetScore()-> void:
	score=0
	emit_signal("score_changed",score)

func startScoreCounting()-> void:
	scoreTimer.start()

func stopScoreCounting()-> void:
	scoreTimer.stop()

func score_timer_timeout()-> void:
	score+=1
	emit_signal("score_changed",score)

func convertToTime(number: int)-> String:
	var result: String=""
	result+=str(number/60)+":"
	var sec=number%60
	if(sec<10):
		result+="0"+str(sec)
	else:
		result+=str(sec)
	return result
