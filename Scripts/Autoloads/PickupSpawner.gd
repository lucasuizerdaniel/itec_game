extends Node

var pickupsScenes: Array[PackedScene]

func _ready() ->void:
	pickupsScenes.push_back(preload("res://Pickups/PickupBullet.tscn"))
	pickupsScenes.push_back(preload("res://Pickups/PickupShell.tscn"))
	pickupsScenes.push_back(preload("res://Pickups/PickupExplosive.tscn"))
	pickupsScenes.push_back(preload("res://Pickups/PickupHealthSmall.tscn"))
	pickupsScenes.push_back(preload("res://Pickups/PickupHealthBig.tscn"))

func putPickupAt(position) ->void:
	var pickup=pickupsScenes.pick_random().instantiate()
	pickup.global_position=position
	get_tree().get_root().call_deferred("add_child",pickup)
