extends Node

var path_to_scores="user://scores.sav"
var scoreEntries: Array[ScoreEntry]

func _ready():
	for i in 10:
		scoreEntries.push_back(ScoreEntry.new())

func loadScoreEntries() ->void:
	if(FileAccess.file_exists(path_to_scores)):
		var file=FileAccess.open(path_to_scores,FileAccess.READ)
		#Load name and score
		for i in 10:
			scoreEntries[i].playerName=file.get_var()
			scoreEntries[i].playerScore=file.get_var()
		#Order by playerScore ASC
		scoreEntries.sort_custom(func(a,b): return a.playerScore>b.playerScore)
		#Set position on leader board
		for i in 10:
			scoreEntries[i].playerPosition=i+1
	else:
		for i in 10:
			scoreEntries[i].playerName="NOBODY"
			scoreEntries[i].playerScore=0
		saveScoreEntries()

func saveScoreEntries() ->void:
	var file=FileAccess.open(path_to_scores,FileAccess.WRITE)
	#For testing make random scores
	#for i in 10:
	#	file.store_var("LSD")
	#	file.store_var(randi_range(1,360))
	for score in scoreEntries:
		file.store_var(score.playerName)
		file.store_var(score.playerScore)

func addScore(newEntry: ScoreEntry)-> void:
	loadScoreEntries()
	scoreEntries.push_back(newEntry)
	
	#Order by playerScore ASC
	scoreEntries.sort_custom(func(a,b): return a.playerScore>b.playerScore)
	
	scoreEntries.pop_back()
	saveScoreEntries()

func checkScore(score: int)-> bool:
	loadScoreEntries()
	var result: bool=true
	if(scoreEntries.back().playerScore>=score):
		result=false
	return result
