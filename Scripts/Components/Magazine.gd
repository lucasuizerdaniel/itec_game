extends Node
class_name Magazine

@export var ammoType: Global.AMMO_TYPES=Global.AMMO_TYPES.INFINITY
@export var ammoMax: int=0
@onready var ammoCurrent: int=ammoMax

signal ammo_changed(current,type)

func useAmmo() ->void:
	ammoCurrent-=1
	emit_signal("ammo_changed",ammoCurrent,ammoType)

func reloadAmmo(ammoReserve: AmmoReserve) ->void:
	var missingAmmo: int=ammoMax-ammoCurrent
	if(ammoType!=Global.AMMO_TYPES.INFINITY):
		if(ammoReserve.getAmmo(ammoType)>=missingAmmo):
			ammoReserve.setAmmo(ammoType,ammoReserve.getAmmo(ammoType)-missingAmmo)
			ammoCurrent=ammoMax
		else:
			ammoCurrent+=ammoReserve.getAmmo(ammoType)
			ammoReserve.setAmmo(ammoType,0)
	else:
		ammoCurrent=ammoMax
	emit_signal("ammo_changed",ammoCurrent,ammoType)
