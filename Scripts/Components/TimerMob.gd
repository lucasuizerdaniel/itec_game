extends Timer
class_name TimerMob

@export var active: bool=true
@export var mobScene: PackedScene
@export var quantity: int=1
@export var updateQuantityTimes: Array[int]

signal spawn(mob: PackedScene,quantityMob: int)

func _ready():
	start()
	connect("timeout",restart)
	if(!updateQuantityTimes.is_empty()):
		Global.connect("score_changed",updateQuantity)

func restart() ->void:
	if(active):
		start()
		emit_signal("spawn",mobScene,quantity)

func deactive() ->void:
	active=false
	stop()

func reactive() ->void:
	active=true
	start()

func updateQuantity(score: int)-> void:
	if(score>=updateQuantityTimes.front()):
		quantity+=1
		updateQuantityTimes.pop_front()
		if(updateQuantityTimes.is_empty()):
			Global.disconnect("score_changed",updateQuantity)
