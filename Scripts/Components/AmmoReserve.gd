extends Node
class_name AmmoReserve

var ammoQuiantities: Array[int]=[]

signal ammo_reserve_changed(type)

func _ready():
	ammoQuiantities.resize(Global.AMMO_TYPES.MAX)
	reset()

func reset() ->void:
	setAmmo(Global.AMMO_TYPES.BULLETS,30)
	setAmmo(Global.AMMO_TYPES.SHELLS,6)
	setAmmo(Global.AMMO_TYPES.EXPLOSIVES,3)

func getAmmo(type: Global.AMMO_TYPES) ->int:
	var result: int=ammoQuiantities[type]
	if(type==Global.AMMO_TYPES.INFINITY):
		result=1
	return result

func setAmmo(type: Global.AMMO_TYPES,newQuantity: int) ->void:
	ammoQuiantities[type]=newQuantity
	emit_signal("ammo_reserve_changed",type)
