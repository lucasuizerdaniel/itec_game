extends Area2D
class_name HitBox

@export var health: Health

func processDamage(damage: float) ->void:
	health.takeDamage(damage)
