extends Node
class_name Health

@export var hpMax: float=0
@onready var hpCurrent: float=hpMax

signal hp_changed(current)

func _ready():
	emit_signal("hp_changed",hpCurrent)

func takeDamage(damage: float) ->void:
	hpCurrent-=damage
	emit_signal("hp_changed",hpCurrent)
func takeHealing(healing: float) ->void:
	hpCurrent+=healing
	if(hpCurrent>hpMax):
		hpCurrent=hpMax
	emit_signal("hp_changed",hpCurrent)
