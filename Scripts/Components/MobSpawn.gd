extends Node
class_name MobSpawn

@onready var _mobPath: Path2D=$Path2D

func _ready():
	for child in get_children():
		if(child is TimerMob):
			child.connect("spawn",spawnMob)

func spawnMob(mobScene: PackedScene,quantity: int) ->void:
	if(get_tree().get_nodes_in_group("mobs").size()<=35):
		for i in quantity:
			var mob=mobScene.instantiate()
			var mobLocation=_mobPath.get_node("PathFollow2D")
			mobLocation.progress_ratio=randf()
			mob.position=mobLocation.position
			get_parent().add_child(mob)

func deactiveMobTimers() ->void:
	for child in get_children():
		if(child is TimerMob):
			child.deactive()
