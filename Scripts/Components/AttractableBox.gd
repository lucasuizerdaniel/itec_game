extends Area2D
class_name AttractableBox

var attractionBox: AttractionBox=null

func moveOwnerTo(box: AttractionBox)-> void:
	attractionBox=box

func _process(delta):
	if(attractionBox!=null):
		var direction=position-attractionBox.position
		get_parent().position+=direction*delta*attractionBox.force
