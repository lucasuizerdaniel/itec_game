extends Marker2D
class_name ProjectileSpawn

@export var projectile: PackedScene
@export var projectileNr: int=1
@export_range(0,360) var arc: float=0

func spawnProjectil() ->void:
	for i in projectileNr:
		var newProjectile=projectile.instantiate()
		newProjectile.global_position=global_position
		if(projectileNr==1):
			newProjectile.global_rotation=global_rotation
		else:
			var arcRad=deg_to_rad(arc)
			var increment=arcRad/(projectileNr-1)
			newProjectile.global_rotation=(global_rotation+increment*i-arcRad/2)
		get_tree().get_root().add_child(newProjectile)
