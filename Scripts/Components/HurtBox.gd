extends Area2D
class_name HurtBox

@export var damage: float=0

func _ready():
	connect("area_entered",_on_area_entered)

func _on_area_entered(area) ->void:
	if(area is HitBox):
		area.processDamage(damage)

func setDisable(isDisabled: bool) ->void:
	get_node("CollisionShape2D").set_deferred("disabled",isDisabled)
