extends Area2D
class_name AttractionBox

@export var force: int=0
var entitiesToMove: Array[Node2D]

func _ready()-> void:
	connect("area_entered",addEntity)
	connect("body_entered",addEntity)
	connect("area_exited",removeEntity)
	connect("body_exited",removeEntity)

func addEntity(entity) ->void:
	if(entity!=get_parent()):
		entitiesToMove.push_back(entity)

func removeEntity(entity) ->void:
	var positionToRemove=entitiesToMove.find(entity)
	if(positionToRemove>=0):
		entitiesToMove.remove_at(positionToRemove)

func _process(delta):
	for entity in entitiesToMove:
		var direction=entity.global_position.direction_to(global_position)
		entity.position+=direction*force*delta
