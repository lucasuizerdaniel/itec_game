extends Control

@export var selector: Node2D
var weapons: Dictionary={
	"Shotgun":preload("res://Weapons/Shotgun.tscn"),
	"Smg":preload("res://Weapons/Smg.tscn"),
	"Rocket Launcher":preload("res://Weapons/RocketLauncher.tscn")}
var weaponOptions: Array[String]=["Shotgun","Smg","Rocket Launcher"]
var index: int=0

signal option_taked

func _ready():
	updateLabel()

func _on_btn_next_pressed():
	if(index<weaponOptions.size()-1):
		index+=1
	else:
		index=0
	updateLabel()

func _on_btn_prev_pressed():
	if(index>0):
		index-=1
	else:
		index=weaponOptions.size()-1
	updateLabel()

func _on_btn_take_pressed():
	var gun=weapons[weaponOptions[index]].instantiate()
	selector.add_child(gun)
	gun.hide()
	weaponOptions.remove_at(index)
	index=0
	updateLabel()
	emit_signal("option_taked")

func updateLabel() ->void:
	if(!weaponOptions.is_empty()):
		$Panel/Label.text=weaponOptions[index]
		match(weaponOptions[index]):
			"Smg":
				$Panel/Sprite2D.region_rect=Rect2(656,144,16,16)
			"Shotgun":
				$Panel/Sprite2D.region_rect=Rect2(624,144,16,16)
			"Rocket Launcher":
				$Panel/Sprite2D.region_rect=Rect2(688,144,16,16)
