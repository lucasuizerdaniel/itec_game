extends Node2D
class_name Weapon

@export var isAutomatic: bool
@onready var _projectileSpawner: ProjectileSpawn=$ProjectileSpawn
@onready var _magazine: Magazine=$Magazine
@onready var _animationTree: AnimationTree=$AnimationTree
var ammoReserveToUse: AmmoReserve
var canAttack: bool=true

func _ready():
	_magazine.connect("ammo_changed",_process_ammo_changes)

func _process(_delta):
	if(get_global_mouse_position().x<get_global_position().x):
		_animationTree.set("parameters/Idle/blend_position",-1.0)
		_animationTree.set("parameters/Reload/blend_position",-1.0)
		_animationTree.set("parameters/Shoot/blend_position",-1.0)
	else:
		_animationTree.set("parameters/Idle/blend_position",1.0)
		_animationTree.set("parameters/Reload/blend_position",1.0)
		_animationTree.set("parameters/Shoot/blend_position",1.0)

func _process_ammo_changes(ammo,_type) ->void:
	if(ammo>0):
		canAttack=true
	else:
		canAttack=false

func performAttack() ->void:
	setIsReloading()
	if(canAttack):
		_projectileSpawner.spawnProjectil()
		_magazine.useAmmo()
		if(!isAutomatic):
			canAttack=false
	if(_magazine.ammoCurrent<=0 && ammoReserveToUse.getAmmo(_magazine.ammoType)>0):
		setIsReloading(true)

func performReload() ->void:
	_magazine.reloadAmmo(ammoReserveToUse)
	setIsReloading()

func semiAction() ->void:
	if(!isAutomatic && _magazine.ammoCurrent>0):
		canAttack=true

func setIsAttacking(value: bool=false) ->void:
	_animationTree["parameters/conditions/isAttacking"]=value
func setIsReloading(value: bool=false) ->void:
	if(value):
		Mouse.changeMouseToReload()
	else:
		Mouse.changeMouseToAttack()
	_animationTree["parameters/conditions/isReloading"]=value

func makeAttack() ->void:
	if(canAttack):
		setIsAttacking(true)

func makeReload() ->void:
	if(ammoReserveToUse.getAmmo(_magazine.ammoType)>0 || _magazine.ammoType==Global.AMMO_TYPES.INFINITY):
		setIsReloading(true)
