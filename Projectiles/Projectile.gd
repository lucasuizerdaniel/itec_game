extends CharacterBody2D
class_name Projectile

@export var impactPatern: ProjectileImpact
@export var speedDefault: int=0
@onready var speedCurrent: int=speedDefault
@onready var _lifeSpan: Timer=$Timer

func _ready():
	add_to_group("projectiles")
	impactPatern.connect("stop_movement",stop)
	impactPatern.connect("start_movement",moveAgain)
	impactPatern.connect("destroy_projectile",endLife)
	_lifeSpan.connect("timeout",endLife)

func endLife() ->void:
	queue_free()

func moveAgain() ->void:
	speedCurrent=speedDefault

func stop() ->void:
	speedCurrent=0

func _physics_process(delta):
	var direction=Vector2.RIGHT.rotated(rotation)
	var collisionResult=move_and_collide(direction*speedCurrent*delta)
	if(collisionResult!=null):
		impactPatern.impactPerform(collisionResult)
