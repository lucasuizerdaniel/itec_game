extends ProjectileImpact
class_name ProjectileImpact_Explosive

@onready var _hurtBox: HurtBox=$HurtBox

func preparations() ->void:
	_hurtBox.setDisable(true)

func impactPerform(_node) ->void:
	emit_signal("stop_movement")
	_hurtBox.setDisable(false)
	await get_tree().create_timer(0.01).timeout
	#_hurtBox.setDisable(true)
	var particles=impactPaticles.instantiate() as GPUParticles2D
	particles.global_position=global_position
	get_tree().get_root().add_child(particles)
	await get_tree().create_timer(0.1).timeout
	emit_signal("destroy_projectile")
	AudioManager.playSoundSFX("res://Assets/Sounds/Explosion.wav")
