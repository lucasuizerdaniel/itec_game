extends ProjectileImpact
class_name ProjectileImpact_Simple

@onready var _hurtBox: HurtBox=$HurtBox

func preparations() ->void:
	_hurtBox.setDisable(true)

func impactPerform(_node) ->void:
	emit_signal("stop_movement")
	_hurtBox.setDisable(false)
	await get_tree().create_timer(0.01).timeout
	emit_signal("destroy_projectile")
	if(impactPaticles):
		var particles=impactPaticles.instantiate() as GPUParticles2D
		particles.global_position=global_position
		particles.rotation=(_node as KinematicCollision2D).get_normal().angle()
		get_tree().get_root().add_child(particles)
