extends Node2D
class_name ProjectileImpact

@export var impactPaticles: PackedScene

signal destroy_projectile
signal stop_movement
signal start_movement

func _ready():
	preparations()

func preparations() ->void:
	pass

func impactPerform(_node) ->void:
	pass
