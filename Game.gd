extends Node

@export var showShop: int=0
var paused: bool=false

func _ready():
	$Player.get_node("AmmoReserve").reset()
	Global.resetScore()
	Global.connect("score_changed",scoreProcess)
	Global.startScoreCounting()

func _input(event):
	if(event.is_action_pressed("pause") && $Player/Health.hpCurrent>0 && !$LayerUI/Shop.is_visible_in_tree()):
		if(!paused):
			paused=true
			Engine.time_scale=0
			$Player.stopCompletely()
			$LayerUI/MenuPause.show()
		else:
			paused=false
			_on_menu_pause_continue_playing()

func _on_player_dead():
	$MobSpawn.deactiveMobTimers()
	Global.stopScoreCounting()
	if(DataManager.checkScore(Global.score)):
		$LayerUI/Hud.hide()
		$LayerUI/AddScoreEntry.setScore(Global.score)
		$LayerUI/AddScoreEntry.show()
	else:
		$LayerUI/MenuGameOver.show()

func scoreProcess(score)-> void:
	if(score==showShop && !$LayerUI/Shop.weaponOptions.is_empty()):
		get_tree().call_group("mobs","queue_free")
		Engine.time_scale=0
		$Player.stopCompletely()
		showShop+=30
		$LayerUI/Shop.show()

func _on_menu_pause_continue_playing():
	Engine.time_scale=1
	$Player.moveAgain()
	$LayerUI/MenuPause.hide()

func _on_menu_game_over_play_again():
	get_tree().call_group("projectiles","queue_free")
	get_tree().call_group("pickups","queue_free")
	get_tree().reload_current_scene()

func _on_shop_option_taked():
	Engine.time_scale=1
	$Player.moveAgain()
	$LayerUI/Shop.hide()
