extends Pickup
class_name PickupHealth

@export var healthRestored: int=0

func effect(_body: Node2D) ->bool:
	var result: bool=false
	if(_body.is_in_group("players")):
		#if(_body.get_node("Health").hpCurrent<_body.get_node("Health").hpMax):
		_body.get_node("Health").takeHealing(healthRestored)
		result=true
	return result
