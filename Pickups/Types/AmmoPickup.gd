extends Pickup
class_name PickupAmmo

@export var ammoType: Global.AMMO_TYPES
@export var quantity: int=0

func effect(_body: Node2D) ->bool:
	if(_body.is_in_group("players")):
		_body.get_node("AmmoReserve").setAmmo(ammoType,_body.get_node("AmmoReserve").getAmmo(ammoType)+quantity)
	return true
