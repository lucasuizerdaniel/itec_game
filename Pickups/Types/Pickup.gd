extends Area2D
class_name Pickup

func _ready():
	z_index=1
	add_to_group("pickups")
	set_collision_layer_value(1,false)
	set_collision_mask_value(1,false)
	set_collision_layer_value(5,true)
	set_collision_mask_value(5,true)
	connect("body_entered",_on_body_entered)

func _on_body_entered(body) ->void:
	if(effect(body)):
		AudioManager.playSoundSFX("res://Assets/Sounds/Pickup.wav")
		queue_free()

func effect(_body: Node2D) ->bool:
	return true
