extends CharacterBody2D

const DEFAULT_SPEED: int=500
var speed: int
var weaponSelector
var currentWeapon: Weapon
var canInteract: bool=true

signal dead

func _ready():
	speed=DEFAULT_SPEED
	weaponSelector=$Center/WeaponSelector
	currentWeapon=weaponSelector.selectedWeapon
	currentWeapon.ammoReserveToUse=$AmmoReserve

func _physics_process(_delta):
	var direction=Input.get_vector("ui_left","ui_right","ui_up","ui_down")
	velocity=direction*speed
	move_and_slide()
	if(canInteract):
		$Center.look_at(get_global_mouse_position())
		if(Input.is_action_pressed("shoot")):
			currentWeapon.makeAttack()

func _input(event):
	if(canInteract):
		if(event.is_action_released("shoot")):
			currentWeapon.semiAction()
		if(event.is_action_pressed("reload")):
			currentWeapon.makeReload()
		if(event.is_action_pressed("change_weapon_up")):
			currentWeapon=weaponSelector.getNextWeapon()
			currentWeapon.ammoReserveToUse=$AmmoReserve
		if(event.is_action_pressed("change_weapon_down")):
			currentWeapon=weaponSelector.getPrevWeapon()
			currentWeapon.ammoReserveToUse=$AmmoReserve

func _on_health_hp_changed(current):
	if(current<=0):
		stopCompletely()
		emit_signal("dead")

func stopCompletely() ->void:
	speed=0
	canInteract=false

func moveAgain() ->void:
	speed=DEFAULT_SPEED
	canInteract=true
