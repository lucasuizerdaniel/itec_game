extends Node2D
var selectedWeapon: Weapon

signal weapon_switched(weapon: Weapon)

func _ready():
	selectedWeapon=get_child(0)
	selectedWeapon.show()
	emit_signal("weapon_switched",selectedWeapon)

func getNextWeapon() ->Weapon:
	var index: int=selectedWeapon.get_index()
	index+=1
	if(index>get_child_count()-1):
		index=0
	switchWeaponTo(index)
	return selectedWeapon

func getPrevWeapon() ->Weapon:
	var index: int=selectedWeapon.get_index()
	index-=1
	if(index<0):
		index=get_child_count()-1
	switchWeaponTo(index)
	return selectedWeapon

func switchWeaponTo(indexWeapon: int) ->void:
	selectedWeapon.hide()
	selectedWeapon=get_child(indexWeapon)
	selectedWeapon.show()
	emit_signal("weapon_switched",selectedWeapon)
