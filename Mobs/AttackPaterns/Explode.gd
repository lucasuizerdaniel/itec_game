extends MobAttack
class_name MobAttack_Explode

@onready var _hurtBox: HurtBox=$HurtBox
@onready var _triggerArea: Area2D=$TriggerArea
@export var healthOwner: Health
@export var animations: AnimationTree
@export var explosionParticles: PackedScene

func _ready():
	_hurtBox.setDisable(true)
	_triggerArea.connect("body_entered",performExplosion)
	healthOwner.connect("hp_changed",on_hp_changed)

func on_hp_changed(current) ->void:
	if(current<=0):
		_hurtBox.set_collision_mask_value(4,false)

func executeAttack() ->void:
	_hurtBox.setDisable(false)
	var particles=explosionParticles.instantiate()
	particles.global_position=mob.global_position
	get_tree().get_root().add_child(particles)

func performExplosion(_node) ->void:
	animations["parameters/conditions/isDead"]=true
