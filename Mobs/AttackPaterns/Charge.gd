extends MobAttack
class_name MobAttack_Charge

@export var restTime: float=0.0
@export var speedCharge: int=0
@onready var _ray: RayCast2D=$RayCast2D
@onready var _hurtBox: HurtBox=$HurtBox
@onready var _timerToRest: Timer=$Timer
var canCharge: bool=true
var isCharging: bool=false

func _ready():
	_hurtBox.setDisable(true)
	_hurtBox.connect("area_entered",rest)
	_timerToRest.connect("timeout",rest)

func _process(_delta):
	var direction=(mob.targetPlayer.global_position-mob.global_position).normalized()
	_ray.target_position=direction*200
	if(_ray.is_colliding() && canCharge):
		executeAttack()

func executeAttack() ->void:
	if(!isCharging):
		_hurtBox.setDisable(false)
		mob.speedCurrent=speedCharge
		isCharging=true
		_timerToRest.start()

func rest(_node: Node=null) ->void:
	_hurtBox.setDisable(true)
	isCharging=false
	canCharge=false
	mob.stopMovement()
	await get_tree().create_timer(restTime).timeout
	canCharge=true
	mob.moveAgain()
