extends MobAttack
class_name MobAttack_SimpleMelee

@onready var _hurtBox: HurtBox=$HurtBox
@onready var _timerAttack: Timer=$Timer

func _ready():
	_hurtBox.connect("area_entered",_on_area_entered)
	_timerAttack.connect("timeout",prepareAttack)

func _on_area_entered(_body) ->void:
	executeAttack()

func executeAttack() ->void:
	_hurtBox.setDisable(true)
	_timerAttack.start()
	mob.stopMovement()

func prepareAttack() ->void:
	_hurtBox.setDisable(false)
	mob.moveAgain()
