extends MobAttack
class_name MobAttack_SimpleShoot

@export var nrShoots: int=1
@export var timeBetweenShoots: float=0
@onready var _shootingAngle: Marker2D=$Marker2D
@onready var _projectileSpawner: ProjectileSpawn=$Marker2D/ProjectileSpawn
@onready var _timerShoot: Timer=$Timer

func _ready():
	_timerShoot.connect("timeout",executeAttack)

func _process(_delta):
	_shootingAngle.look_at(mob.targetPlayer.global_position)

func executeAttack() ->void:
	for i in nrShoots:
		_projectileSpawner.spawnProjectil()
		await get_tree().create_timer(timeBetweenShoots).timeout
	_timerShoot.start()
