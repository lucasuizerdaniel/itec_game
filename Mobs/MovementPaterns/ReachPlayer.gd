extends MobMovement
class_name MobMovement_ReachPlayer

@export var agent: NavigationAgent2D

func _physics_process(_delta):
	agent.set_target_position(mob.targetPlayer.global_position)
	var currentAgentPosition=mob.global_position
	var nextAgentPosition=agent.get_next_path_position()
	mob.velocity=currentAgentPosition.direction_to(nextAgentPosition)*mob.speedCurrent
	mob.move_and_slide()
