extends MobMovement
class_name MobMovement_ChasePlayer

func _physics_process(_delta):
	var direction=mob.global_position.direction_to(mob.targetPlayer.global_position)
	mob.velocity=direction*mob.speedCurrent
	mob.move_and_slide()
