extends MobMovement
class_name MobMovement_Wander

var direction
var changeDirectionTime: float=0

func _physics_process(_delta):
	if(changeDirectionTime<=0):
		changeDirectionTime=randf_range(1,3)
		direction=Vector2(randf_range(-1,1),randf_range(-1,1)).normalized()
	else:
		changeDirectionTime-=_delta
	mob.velocity=direction*mob.speedCurrent
	mob.move_and_slide()
