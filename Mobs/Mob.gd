extends CharacterBody2D
class_name Mob

@export var speedDefault: int=0
@onready var speedCurrent: int=speedDefault
@onready var _health=$Health
@onready var _animations: AnimationTree=$AnimationTree
var targetPlayer: CharacterBody2D
@onready var deadParticles =preload("res://Particles/DeadParticles.tscn")

func _ready():
	targetPlayer=get_tree().get_first_node_in_group("players")
	z_index=2
	add_to_group("mobs")
	_health.connect("hp_changed",_on_hp_changed)

func _on_hp_changed(current) ->void:
	if(current<=0):
		if(randi_range(1,10)>=6):
			PickupSpawner.putPickupAt(global_position)
		stopMovement()
		_animations["parameters/conditions/isDead"]=true
		var particles=deadParticles.instantiate()
		particles.global_position=global_position
		get_tree().get_root().add_child(particles)
	else:
		setIsHitted(true)

func stopMovement() ->void:
	speedCurrent=0
	_animations["parameters/conditions/isMoving"]=false
	_animations["parameters/conditions/isIdle"]=true

func moveAgain() ->void:
	speedCurrent=speedDefault
	_animations["parameters/conditions/isMoving"]=true
	_animations["parameters/conditions/isIdle"]=false

func setIsHitted(value: bool=false) ->void:
	_animations["parameters/conditions/isHitted"]=value
